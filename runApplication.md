# Start the VENV and the app+api server
source venv/bin/activate.fish && uvicorn api:api --reload
---
# Entering the DB postgreesql
pssql postgres

# Start the postgreesql
brew services start postgresql
# Stop the postgreesql
pg_ctl stop -D /usr/local/var/postgresql@14/
brew services stop postgresql

# Show status
pg_ctl status -D /usr/local/var/postgresql@14/
