# Ortho-fast-api

Here will be python3 Fast API based service providing usage of TRG-decoding for Gadzhiev Islam magistracy diploma work.

*COMMANDS TO START THE PROJECT*:
1. run VENV - `source venv/bin/activate` for bash or `source venv/bin/activate.fish` for fish (my own local env)
2. run the app + api - `uvicorn api:api --reload` (first name for API, second for APP)

---
*CURRENT PAGES & INTERAFCES*:
`/app` - main APP page
`/app/info` - detailed INFO page
`/api` - main GET response for REST API
`/docs` - SWAGGER
`/api/post-image` - FastAPI image posting
`/upload-image` - FLASK side image uploading (in-progress)
``

- Server is up on Uvicorn
---
- [x] Rise initial FAST-API service and server
- [] Assert docs
- [x] Probably in nearest future this would be integrated with FLASK app
- [in progress] Thus, need to introduce web-interface and business logic (here will be App Diagram)
- [in progress] Image uploading:
1. [x] Client form
2. [in progress] Uploading to the client app
3. [x] Uploading image to the server (api) filesystem 
4. [] Saving data-info into the database
- [] Resulting image receiving
