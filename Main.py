import os
from math import atan
import csv
from pandas import read_csv as pandasReadCSV
from pandas import concat as pdConcat
from pandas import DataFrame
from pandas import Series
import imutils
import numpy as np
import cv2
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import math


img = cv2.imread('samples/cutted.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
hsvImg = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

lightSelector = (0, 0, 0)
darkSelector = (140, 140, 140)

mask = cv2.inRange(hsvImg, lightSelector, darkSelector)
result = cv2.bitwise_and(img, img, mask = mask)

# Noice reduction
dst = cv2.fastNlMeansDenoising(mask, None, 5, 13, 31)

# Prepocess
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.medianBlur(gray, 45)
flag, thresh = cv2.threshold(blur, 125, 220, cv2.THRESH_BINARY)
thresh = cv2.erode(thresh, None, iterations = 2)
thresh = cv2.dilate(thresh, None, iterations = 2)

# Find contours
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
contours = max(contours, key = cv2.contourArea)

imgCopy = img.copy()
cv2.drawContours(imgCopy, contours, -1, (0, 255, 0), 2, cv2.LINE_AA)

# determine the most extreme points along the contour
extLeft = tuple(contours[contours[:, :, 0].argmin()][0])
extRight = tuple(contours[contours[:, :, 0].argmax()][0])
extTop = tuple(contours[contours[:, :, 1].argmin()][0])
extBot = tuple(contours[contours[:, :, 1].argmax()][0])

# Select long perimeters only
perimeters = [cv2.arcLength(contours[i],True) for i in range(len(contours))]
listindex =[i for i in range(15) if perimeters[i]>perimeters[0]/2]
numcards =len(listindex)

# Show image
imgcont = img.copy()
# [cv2.drawContours(imgcont, [contours[i]], 0, (255,255,0), 5) for i in listindex]
cv2.drawContours(imgcont, [contours], -1, (0, 255, 255), 2)
cv2.imwrite('withContours.jpg', imgcont)

##########################

alphabeticList = [
    'EA',
    'EB',
    'EC',
    'ED',
    'EE',
    'EF',
    'EG',
    'EH',
    'EI',
    'EJ',
    'EK',
    'EL',
    'EM',
    'EN',
]
# Approximating Contours and Finding their Convex hull
accuracy = 0.005 * cv2.arcLength(contours, True)
approx = cv2.approxPolyDP(contours, accuracy, True)
approxShortened = approx[25:]

with open('EmptyApproxPoints.csv', 'w', newline = '') as file:
    myWriter = csv.writer(file, delimiter = ',')
    myWriter.writerows(approxShortened)

dataFrame = pandasReadCSV('EmptyApproxPoints.csv')
dataFrame['Names'] = Series(alphabeticList)
dataFrame.to_csv('approxNamedPoints.csv', index = False)

imgPolyDP = imgcont.copy()
cv2.drawContours(imgcont, [approxShortened], 0, (0,255,0), 2)
cv2.imwrite('withApproxPolyDP.jpg', imgPolyDP)

##########################

# draw the outline of the object, then draw each of the
# extreme points, where the left-most is red, right-most
# is green, top-most is blue, and bottom-most is teal

def slope(x1, y1, x2, y2):
    return (float)(y2-y1)/(x2-x1)

def findAngle(M1, M2, text = ''):
    PI = 3.14159265
    angle = abs((M2 - M1) / (1 + M1 * M2))
    ret = atan(angle)
    val = (ret * 180) / PI
    print(round(val, 4), text)
    return f'{round(val, 4)}, {text}'

FONT_HERSHEY_SIMPLEX = cv2.FONT_HERSHEY_SIMPLEX

imgCopyForExtremePoints = imgPolyDP.copy()
completeImageToPlot = cv2.cvtColor(imgCopyForExtremePoints, cv2.COLOR_BGR2RGB)

circlesArr = [extLeft, extRight, extTop, extBot]
for circleToDraw in approxShortened:
    cv2.circle(completeImageToPlot, (circleToDraw[0][0], circleToDraw[0][1]), 5, (0, 0, 255), 3) # Center
    for extremeCircle in circlesArr:
        cv2.circle(completeImageToPlot, extremeCircle, 5, (0, 255, 255), 3) # Center
    pass

# Example line: N-PM
x1 = approxShortened[len(approxShortened) - 2].ravel()[0]
y1 = approxShortened[len(approxShortened) - 2].ravel()[1]
x2 = approxShortened[len(approxShortened) - 9].ravel()[0]
y2 = approxShortened[len(approxShortened) - 9].ravel()[1]
cv2.line(completeImageToPlot, (x1, y1), (x2, y2), (200, 0, 0), thickness = 2, lineType = 8)
cv2.putText(completeImageToPlot, 'N', (x1, y1), FONT_HERSHEY_SIMPLEX, 1, (250, 0, 0), 2, lineType = 8)
cv2.putText(completeImageToPlot, 'PM', (x2, y2), FONT_HERSHEY_SIMPLEX, 1, (250, 0, 0), 2, lineType = 8)

# Example line: PM-Xi
x4 = approxShortened[len(approxShortened) - 12].ravel()[0]
y4 = approxShortened[len(approxShortened) - 12].ravel()[1]
cv2.line(completeImageToPlot, (x2, y2), (x4, y4), (200, 0, 0), thickness = 2, lineType = 8)
cv2.putText(completeImageToPlot, 'Xi', (x4, y4), FONT_HERSHEY_SIMPLEX, 1, (250, 0, 0), 2, lineType = 8)

M1 = slope(x1, y1, x2, y2)
M2 = slope(x2, y2, x4, y4)
resAngel = findAngle(M1, M2, 'градусов: N-PM-Xi')
cv2.imwrite('result-image.jpg', completeImageToPlot)
os.system("open 'result.html' -a Firefox")