import os
import requests
import logging
from fastapi import FastAPI, Request, UploadFile, File
from fastapi.middleware.wsgi import WSGIMiddleware
from fastapi import Body, HTTPException, Response
from fastapi.exceptions import RequestValidationError
from fastapi.routing import APIRoute

from flask import Flask, render_template, flash, request, redirect, url_for

import uvicorn
import shutil
from typing import Callable, List

ALLOWED_IMAGE_EXTENSIONS = ['png', 'jpeg', 'jpg']

def isUploadFileAllowed(filename):
   return '.' in filename and filename.split('.', 1)[1].lower() in ALLOWED_IMAGE_EXTENSIONS

class ValidationErrorLoggingRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            try:
                print('HERE COMES RIGHT REQUEST LOG')
                return await original_route_handler(request)
            except Exception as exc:
                body = await request.body()
                detail = {"errors": exc.errors(), "body": body.decode()}
                print('HERE COMES ERROR REQUEST LOG')
                raise HTTPException(status_code=422, detail=detail)

        return custom_route_handler

api = FastAPI()
api.router.route_class = ValidationErrorLoggingRoute

flaskApp = Flask(__name__)
# All after /app is for APP, /api for API
api.mount('/app', WSGIMiddleware(flaskApp))

async def catch_exceptions_middleware(request: Request, call_next):
    try:
        print('works for middleware')
        return await call_next(request)
    except Exception:
        print(logging.exception('ERROR WAS THROWN'))
        return Response("Internal server error", status_code=500)

api.middleware('http')(catch_exceptions_middleware)

@api.get('/api')
def index():
    return {'text': 'API works'}

@api.post("/post-image")
async def postImage(file: UploadFile = File(...)):
    with open(f"static/uploads/{file.filename}", 'wb') as buffer:
        shutil.copyfileobj(file, buffer)
    successMessage = 'Изображение загружено успешно, пожалуйста, ожидайте ответа сервера…'
    return {'success': True, 'filename': file, 'message': successMessage}

@flaskApp.route('/')
def index_page(sent = False, result = None):
    if sent == True:
        return render_template('index.html', result=result)
    else:
        return render_template('index.html')

@flaskApp.route('/info')
def info_page():
    return 'FLASK info page found'

@flaskApp.route('/upload', methods=['POST'])
async def upload():
    if not request.files:
        return {'NO-FILES': True}
    if 'file' not in request.files:
        flash('No file received')
        return {'received': False}
    file = request.files['file']
    if file.filename == '':
        flash('No image was selected for uploading')
        return redirect(request.url)
    if not isUploadFileAllowed(file.filename):
        return {'MESSAGE': 'File extension is not allowed'}
    result = await postImage(request.files['file'])
    os.system('python3 Main.py')
    return display('result-image.jpg')

@flaskApp.route('/display/<filename>')
def display(filename):
    return redirect(url_for('static', filename = filename), code = 301)

if __name__ == '__main__':
    uvicorn.run(api, host = 'localhost', port = 8000)
