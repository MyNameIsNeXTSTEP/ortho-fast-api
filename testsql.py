from tabulate import tabulate
from pandas import DataFrame
import psycopg2

dbName = 'trg_db'
dbSchema = 'test_schema_0'
tableName = 'table_main_view'

connection = psycopg2.connect(user="mac",
                                  password="",
                                  host="localhost",
                                  port="5432",
                                  database=dbName,
                                  options=f'-c search_path={dbSchema}')
cursor = connection.cursor()
cursor.execute(f'SELECT * FROM {tableName};')
recivedTableInfo = cursor.fetchall()

column_names = []
for elt in cursor.description:
    column_names.append(elt[0])
df = DataFrame(recivedTableInfo, columns=column_names)

print(f'\nDatabase name: {dbName}\nSchema name: {dbSchema}\nTable name: {tableName}\n')
print(df.to_markdown(), '\n')
